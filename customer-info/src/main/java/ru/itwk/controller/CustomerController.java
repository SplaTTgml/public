package ru.itwk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itwk.model.Customer;
import ru.itwk.model.CustomersDTO;
import ru.itwk.service.CustomerService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/customer")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getCustomerByFullName(
            @QueryParam("firstName") String firstName,
            @QueryParam("secondName") String secondName,
            @QueryParam("patronymic") String patronymic) {
        final Customer customer = customerService.getCustomerByFullName(firstName, secondName, patronymic);
        return Response.status(Response.Status.OK).entity(customer).build();
    }

    @GET
    @Path("/all")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getAllCustomers() {
        final CustomersDTO dto = new CustomersDTO(customerService.getAllCustomers());
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getCustomerById(@PathParam("id") Long id) {
        final Customer customer = customerService.getCustomerById(id);
        return Response.status(Response.Status.OK).entity(customer).build();
    }
}

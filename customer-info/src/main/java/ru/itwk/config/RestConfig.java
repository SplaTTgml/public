package ru.itwk.config;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import ru.itwk.controller.CustomerController;
import ru.itwk.exception.handler.GlobalExceptionHandler;
import ru.itwk.exception.handler.NotFoundExceptionHandler;

public class RestConfig extends ResourceConfig {

    public RestConfig() {
        register(JacksonFeature.class);
        register(CustomerController.class);
        register(NotFoundExceptionHandler.class);
        register(GlobalExceptionHandler.class);
    }
}

package ru.itwk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itwk.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findCustomerByFirstNameAndSecondNameAndPatronymicIgnoreCase(String firstName, String secondName, String patronymic);
}

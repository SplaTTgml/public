package ru.itwk.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.itwk.exception.CustomerNotFoundException;
import ru.itwk.model.ResponseError;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<CustomerNotFoundException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotFoundExceptionHandler.class);

    public Response toResponse(CustomerNotFoundException e) {
        LOGGER.info(e.getMessage(), e);
        return Response.status(Response.Status.NOT_FOUND).entity(
                ResponseError.getBuilder()
                        .setError(e.getMessage())
                        .setStatus(Response.Status.NOT_FOUND)
                        .build()
        ).type(MediaType.APPLICATION_JSON)
                .build();
    }
}
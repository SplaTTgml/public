package ru.itwk.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.itwk.model.ResponseError;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GlobalExceptionHandler implements ExceptionMapper<Throwable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    private static final String ERROR = "GENERAL ERROR HAS OCCURED";

    @Override
    public Response toResponse(Throwable e) {
        LOGGER.error(e.getMessage(), e);
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(
                ResponseError.getBuilder()
                        .setError(ERROR)
                        .setStatus(Response.Status.SERVICE_UNAVAILABLE)
                        .build()
        ).type(MediaType.APPLICATION_JSON)
                .build();
    }
}

package ru.itwk.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itwk.exception.CustomerNotFoundException;
import ru.itwk.model.Customer;
import ru.itwk.repository.CustomerRepository;
import ru.itwk.service.CustomerService;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class CustomerServiceImpl implements CustomerService {

    private static final String COSTUMERS = "Costumers";
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);
    private static final String SPACE = " ";
    private static final String NOT_FOUND = " not found.";
    private static final String CUSTOMER_WITH_ID = "Customer with id ";

    private CustomerRepository repository;

    @Autowired
    public void setRepository(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Customer getCustomerByFullName(String firstName, String secondName, String patronymic) {
        Customer customer = null;
        try {
            customer = repository.findCustomerByFirstNameAndSecondNameAndPatronymicIgnoreCase(firstName, secondName, patronymic);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        if (customer == null) {
            throw new CustomerNotFoundException(firstName + SPACE + secondName + SPACE + patronymic + NOT_FOUND);
        }
        return customer;
    }

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> customers = null;
        try {
            customers = repository.findAll();
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        if (customers == null || customers.isEmpty()) {
            throw new CustomerNotFoundException(COSTUMERS + NOT_FOUND);
        }
        return customers;
    }

    @Override
    public Customer getCustomerById(Long id) {
        Optional<Customer> optionalCustomer = Optional.empty();
        try {
            optionalCustomer = repository.findById(id);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        if (!optionalCustomer.isPresent()) {
            throw new CustomerNotFoundException(CUSTOMER_WITH_ID + id + NOT_FOUND);
        }
        return optionalCustomer.get();
    }
}

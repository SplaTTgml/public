package ru.itwk.service;

import ru.itwk.model.Customer;

import java.util.List;

/**
 * Implementing this interface allows an object gets access to operations with {@link Customer}.
 *
 * @author Ihar Yukhalka
 */
public interface CustomerService {

    /**
     * Return the customer.
     *
     * @param firstName  first name of customer.
     * @param secondName second name of customer.
     * @param patronymic patronymic of customer.
     * @return the costumer with given full name.
     */
    Customer getCustomerByFullName(String firstName, String secondName, String patronymic);

    /**
     * Return all customers.
     *
     * @return all existing customers.
     */
    List<Customer> getAllCustomers();

    /**
     * Return the customer.
     *
     * @param id required customer id
     * @return the customer with given id.
     */
    Customer getCustomerById(Long id);
}

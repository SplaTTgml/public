package ru.itwk.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "first_name")
    private String firstName;
    @NonNull
    @Column(name = "second_name")
    private String secondName;
    @NonNull
    @Column(name = "patronymic")
    private String patronymic;
    @NonNull
    @Column(name = "email")
    private String email;
    @OneToOne(orphanRemoval = true)
    private Balance balance;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "customers_tariffs",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "tariff_id")
    )
    private Set<Tariff> tariffs = new HashSet<>();
}

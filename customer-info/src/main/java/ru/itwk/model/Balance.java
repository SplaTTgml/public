package ru.itwk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Entity
@Table(name = "balance")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Balance implements Serializable {

    private static final String DOT = ".";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @XmlTransient
    private Long id;

    @XmlTransient
    private long amount;

    @NonNull
    @XmlElement(name = "rubs")
    public long getRubs() {
        return amount / 100;
    }

    @NonNull
    @XmlElement(name = "coins")
    public long getCoins() {
        return amount % 100;
    }

    @Override
    public String toString() {
        return amount / 100 + DOT + (amount / 10 % 10) + amount % 10;
    }
}

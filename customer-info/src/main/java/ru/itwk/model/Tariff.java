package ru.itwk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tariff")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Tariff implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @XmlTransient
    private Long id;

    @NonNull
    private String name;
    @NonNull
    private String description;
    @ManyToMany(mappedBy = "tariffs")
    @Transient
    @XmlTransient
    @JsonIgnore
    private Set<Customer> customers = new HashSet<>();
}

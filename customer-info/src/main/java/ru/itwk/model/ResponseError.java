package ru.itwk.model;

import javax.ws.rs.core.Response;

public class ResponseError {

    private int status;
    private String error;

    public static Builder getBuilder() {
        return new ResponseError().new Builder();
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public class Builder {

        private Builder() {
        }

        public Builder setStatus(Response.Status status) {
            ResponseError.this.status = status.getStatusCode();
            return this;
        }

        public Builder setError(String error) {
            ResponseError.this.error = error;
            return this;
        }

        public ResponseError build() {
            return ResponseError.this;
        }
    }
}

drop table if exists customers_tariffs;
drop table if exists customer;
drop table if exists balance;
drop table if exists tariff;

create table balance
(
    id     bigserial not null,
    amount int8      not null,
    primary key (id)
);

create table customer
(
    id          bigserial    not null,
    first_name  varchar(255) not null,
    second_name varchar(255) not null,
    patronymic  varchar(255) not null,
    email       varchar(255) not null,
    balance_id  int8,
    primary key (id)
);
alter table customer
    add constraint customer_balance_id foreign key (balance_id) references balance;

create table tariff
(
    id          bigserial    not null,
    name        varchar(255) not null,
    description varchar(255) not null,
    primary key (id)
);

create table customers_tariffs
(
    customer_id int8 not null,
    tariff_id   int8 not null,
    primary key (customer_id, tariff_id)
);
alter table customers_tariffs
    add constraint customers_tariffs_customer_id foreign key (customer_id) references customer;
alter table customers_tariffs
    add constraint customers_tariffs_tariff_id foreign key (tariff_id) references tariff;
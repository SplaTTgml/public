insert into balance(amount)
values (144);
insert into balance(amount)
values (32);
insert into customer(first_name, second_name, patronymic, email, balance_id)
values ('Igor', 'Yukhalko', 'Mikhailovich', 'igor@gmail.com', 1);
insert into customer(first_name, second_name, patronymic, email, balance_id)
values ('Andrey', 'Pupishkin', 'Valerievich', 'andrey@gmail.com', 2);
insert into tariff(name, description)
values ('LIGHT', 'Cheap cost.');
insert into tariff(name, description)
values ('MEDIUM', 'Optimal cost.');
insert into tariff(name, description)
values ('HARD', 'Expensive cost.');
insert into customers_tariffs(customer_id, tariff_id)
values (1, 1);
insert into customers_tariffs(customer_id, tariff_id)
values (1, 3);
insert into customers_tariffs(customer_id, tariff_id)
values (2, 2);
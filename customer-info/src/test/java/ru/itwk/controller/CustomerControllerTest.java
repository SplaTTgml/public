package ru.itwk.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.itwk.exception.CustomerNotFoundException;
import ru.itwk.model.Customer;
import ru.itwk.service.CustomerService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springContext.xml")
public class CustomerControllerTest {

    private static final Long EXPECTED_ID = 1L;
    private static final String EXPECTED_FIRST_NAME = "Igor";
    private static final String EXPECTED_SECOND_NAME = "Yukhalko";
    private static final String EXPECTED_PATRONYMIC = "Mikhailovich";
    private static final int EXPECTED_SIZE = 2;
    private static final Long WRONG_ID = 3L;

    @Autowired
    private CustomerService service;

    @Test
    public void getCustomerByFullName() {
        final Customer customer = service.getCustomerByFullName(EXPECTED_FIRST_NAME, EXPECTED_SECOND_NAME, EXPECTED_PATRONYMIC);
        assertEquals(EXPECTED_FIRST_NAME, customer.getFirstName());
        assertEquals(EXPECTED_SECOND_NAME, customer.getSecondName());
        assertEquals(EXPECTED_PATRONYMIC, customer.getPatronymic());
    }

    @Test
    public void getAllCustomers() {
        final List<Customer> customers = service.getAllCustomers();
        assertEquals(EXPECTED_SIZE, customers.size());
    }

    @Test
    public void getCustomerById() {
        final Customer customer = service.getCustomerById(EXPECTED_ID);
        assertEquals(EXPECTED_ID, customer.getId());
    }

    @Test
    public void getCustomerWithWrongId() {
        assertThrows(CustomerNotFoundException.class, () -> {
            service.getCustomerById(WRONG_ID);
        });
    }
}
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" omit-xml-declaration="yes"/>
    <xsl:template match="/customers">
        <html>
            <body>
                <xsl:for-each select="customer">
                    <p>
                        <h7>
                            <xsl:value-of select="email"/>
                        </h7>
                        <strong>
                            <xsl:value-of select="id"/>
                        </strong>
                        <span>
                            <xsl:value-of select="balance/rubs * 100 + balance/coins"/>
                        </span>
                    </p>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
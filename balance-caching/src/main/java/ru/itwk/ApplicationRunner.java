package ru.itwk;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itwk.config.BeanName;
import ru.itwk.service.CustomerDataService;
import ru.itwk.task.impl.CacheCustomerDataTask;
import ru.itwk.task.impl.NotifyDebtorTask;

import java.util.Calendar;

public class ApplicationRunner {

    private static final String BASE_PACKAGE = "ru.itwk";

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BASE_PACKAGE);

        CustomerDataService service = context.getBean(BeanName.CUSTOMER_DATA_SERVICE_IMPL, CustomerDataService.class);

        CacheCustomerDataTask cacheCustomerDataTask = context.getBean(BeanName.CACHE_CUSTOMER_DATA_TASK, CacheCustomerDataTask.class);
        NotifyDebtorTask notifyDebtorTask = context.getBean(BeanName.NOTIFY_DEBTOR_TASK, NotifyDebtorTask.class);

        cacheCustomerDataTask.start(service);
        notifyDebtorTask.start(service);

        // FOR TEST
        final AmqpTemplate template = context.getBean(AmqpTemplate.class);
        while (true) {
            printMessageFromRabbit(template);
        }
    }

    private static void printMessageFromRabbit(AmqpTemplate template) {
        String messageFromAMQP = (String) template.receiveAndConvert("debtors");
        if (messageFromAMQP != null) {
            System.err.println("RabbitMQ -> " + Calendar.getInstance().getTime() + ": " + messageFromAMQP);
        }
    }
}

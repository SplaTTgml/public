package ru.itwk.model;

import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ru.itwk.config.BeanName;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.Collections;

@Component(BeanName.XML_REST_TEMPLATE)
public class XmlRestTemplate extends RestTemplate {

    private HttpEntity<MultiValueMap<String, String>> request;

    @PostConstruct
    public void init() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        request = new HttpEntity<>(headers);
    }

    @Override
    public <T> ResponseEntity<T> getForEntity(URI url, Class<T> responseType) {
        return exchange(url, HttpMethod.GET, request, responseType);
    }
}

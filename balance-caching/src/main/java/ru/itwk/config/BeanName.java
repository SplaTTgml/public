package ru.itwk.config;

public final class BeanName {

    public static final String CUSTOMER_DATA_HANDLER = "customerDataHandler";
    public static final String DEBTOR_HANDLER = "debtorHandler";
    public static final String XML_REST_TEMPLATE = "xmlRestTemplate";
    public static final String CUSTOMER_DATA_SERVICE_IMPL = "customerDataServiceImpl";
    public static final String CACHE_CUSTOMER_DATA_TASK = "cacheCustomerDataTask";
    public static final String NOTIFY_DEBTOR_TASK = "notifyDebtorTask";

    private BeanName() {
    }
}

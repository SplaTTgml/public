package ru.itwk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

@Configuration
@EnableCouchbaseRepositories(basePackages = {"ru.itwk.repository"})
@PropertySource("classpath:application.properties")
public class CouchbaseConfiguration extends AbstractCouchbaseConfiguration {

    @Value("${couchbase.url}")
    private String url;
    @Value("${couchbase.username}")
    private String username;
    @Value("${couchbase.password}")
    private String pwd;
    @Value("${couchbase.bucket.name}")
    private String bucketName;
    @Value("${couchbase.auto.index}")
    private boolean isAutoIndex;

    @Override
    public String getConnectionString() {
        return url;
    }

    @Override
    public String getUserName() {
        return username;
    }

    @Override
    public String getPassword() {
        return pwd;
    }

    @Override
    public String getBucketName() {
        return bucketName;
    }

    @Override
    protected boolean autoIndexCreation() {
        return isAutoIndex;
    }
}

package ru.itwk.service;

import ru.itwk.model.CustomerData;

import java.util.List;

/**
 * Implementing this interface allows an object gets access to operations with {@link CustomerData}.
 *
 * @author Ihar Yukhalka
 */
public interface CustomerDataService {

    /**
     * Caching {@link CustomerData}.
     *
     * @param actualData a list of actual {@link CustomerData}.
     */
    void cacheDate(List<CustomerData> actualData);

    /**
     * Checking customers balance and return debtors.
     *
     * @param minimalBalance a minimal balance.
     * @return a list of debtors.
     */
    List<CustomerData> findDebtors(long minimalBalance);
}

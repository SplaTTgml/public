package ru.itwk.service.impl;

import com.couchbase.client.java.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.stereotype.Service;
import ru.itwk.model.CustomerData;
import ru.itwk.repository.CustomerDataRepository;
import ru.itwk.service.CustomerDataService;
import ru.itwk.config.BeanName;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Service(BeanName.CUSTOMER_DATA_SERVICE_IMPL)
public class CustomerDataServiceImpl implements CustomerDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerDataServiceImpl.class);

    @Value("${couchbase.work.period}")
    private long period;

    private CustomerDataRepository repository;
    private CouchbaseTemplate template;

    @Autowired
    public void setTemplate(CouchbaseTemplate template) {
        this.template = template;
    }

    @Autowired
    public void setRepository(CustomerDataRepository repository) {
        this.repository = repository;
    }

    @Override
    public void cacheDate(List<CustomerData> actualData) {
        try {
            repository.saveAll(actualData);
            Collection collection = template.getCouchbaseClientFactory().getDefaultCollection();
            actualData.forEach(customerData -> collection.touch(customerData.getId(), Duration.ofMillis(period)));
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public List<CustomerData> findDebtors(long minimalBalance) {
        List<CustomerData> customerDataList = new ArrayList<>();
        try {
            customerDataList = repository.findCustomerDataByBalanceIsLessThan(minimalBalance);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        return customerDataList;
    }
}

package ru.itwk.task;

import ru.itwk.service.CustomerDataService;

/**
 * Implementing this interface allows an application process {@link ru.itwk.model.CustomerData}.
 *
 * @author Ihar Yukhalka
 */
public interface Task {

    /**
     * This method launches your task.
     *
     * @param service instance {@link CustomerDataService}.
     */
    void start(CustomerDataService service);
}

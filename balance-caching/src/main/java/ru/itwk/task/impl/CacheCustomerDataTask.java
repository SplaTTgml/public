package ru.itwk.task.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import ru.itwk.config.BeanName;
import ru.itwk.handler.CustomerDataHandler;
import ru.itwk.model.XmlRestTemplate;
import ru.itwk.service.CustomerDataService;
import ru.itwk.task.Task;

import javax.xml.XMLConstants;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

@Component(BeanName.CACHE_CUSTOMER_DATA_TASK)
public class CacheCustomerDataTask implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheCustomerDataTask.class);
    private static final String CACHING_LAUNCHED = "Couchbase caching is launched.";
    private static final int DELAY = 0;
    private static final String EMPTY_STRING = "";

    @Value("${couchbase.work.period}")
    private long period;
    @Value("${couchbase.work.uri}")
    private String couchbaseUri;
    @Value("classpath:customers.xslt")
    private Resource xsltFile;

    private XmlRestTemplate xmlRestTemplate;
    private CustomerDataHandler handler;

    @Autowired
    public void setXmlRestTemplate(XmlRestTemplate xmlRestTemplate) {
        this.xmlRestTemplate = xmlRestTemplate;
    }

    @Autowired
    public void setHandler(CustomerDataHandler handler) {
        this.handler = handler;
    }

    @Override
    public void start(CustomerDataService service) {
        TransformerFactory factory = TransformerFactory.newInstance();
        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, EMPTY_STRING);
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, EMPTY_STRING);
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile.getInputStream()));
            updateCache(service, transformer);
            LOGGER.info(CACHING_LAUNCHED);
        } catch (TransformerConfigurationException | IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void updateCache(CustomerDataService service, Transformer transformer) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    ResponseEntity<String> response = xmlRestTemplate.getForEntity(couchbaseUri, String.class);
                    transformer.transform(
                            new StreamSource(new StringReader(Objects.requireNonNull(response.getBody()))),
                            new SAXResult(handler));
                    service.cacheDate(handler.getCustomerDataList());
                } catch (TransformerException | HttpClientErrorException e) {
                    LOGGER.warn(e.getMessage(), e);
                }
            }
        }, DELAY, period);
    }
}

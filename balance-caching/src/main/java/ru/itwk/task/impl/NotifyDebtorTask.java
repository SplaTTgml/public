package ru.itwk.task.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.xml.sax.SAXException;
import ru.itwk.task.Task;
import ru.itwk.handler.DebtorHandler;
import ru.itwk.model.CustomerData;
import ru.itwk.model.XmlRestTemplate;
import ru.itwk.service.CustomerDataService;
import ru.itwk.config.BeanName;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

@Component(BeanName.NOTIFY_DEBTOR_TASK)
public class NotifyDebtorTask implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotifyDebtorTask.class);
    private static final String SUFFIX_DEBTOR_MESSAGE = ", you have a low balance, please make a payment. Current: ";
    private static final int DELAY = 0;
    private static final String RABBIT_LAUNCHED = "RabbitMQ is launched.";

    @Value("${rabbit.work.period}")
    private long period;
    @Value("${rabbit.work.url}")
    private String rabbitUrl;
    @Value("${rabbit.queue.name}")
    private String queueName;
    @Value("${property.minimal-balance}")
    private long minimalBalance;

    private XmlRestTemplate xmlRestTemplate;
    private AmqpTemplate template;
    private DebtorHandler handler;

    @Autowired
    public void setXmlRestTemplate(XmlRestTemplate xmlRestTemplate) {
        this.xmlRestTemplate = xmlRestTemplate;
    }

    @Autowired
    public void setTemplate(AmqpTemplate template) {
        this.template = template;
    }

    @Autowired
    public void setHandler(DebtorHandler handler) {
        this.handler = handler;
    }

    @Override
    public void start(CustomerDataService service) {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = parserFactory.newSAXParser();
            checkDebtorsAndSendMessage(service, saxParser);
            LOGGER.info(RABBIT_LAUNCHED);
        } catch (ParserConfigurationException | SAXException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void checkDebtorsAndSendMessage(CustomerDataService service, SAXParser saxParser) {
        new Timer().schedule(new TimerTask() {

            private static final String DOT = ".";

            @Override
            public void run() {
                service.findDebtors(minimalBalance).parallelStream().forEach(customerData -> {
                    String uri = rabbitUrl + customerData.getCustomerId();
                    try {
                        ResponseEntity<String> response = xmlRestTemplate.getForEntity(uri, String.class);
                        saxParser.parse(new ByteArrayInputStream(Objects.requireNonNull(response.getBody()).getBytes()), handler);
                        String message = handler.getAmqpMessage() + SUFFIX_DEBTOR_MESSAGE + stringBalance(customerData);
                        template.convertAndSend(queueName, message);
                    } catch (SAXException | IOException | HttpClientErrorException e) {
                        LOGGER.warn(e.getMessage(), e);
                    }
                });
            }

            private String stringBalance(CustomerData customerData) {
                long balance = customerData.getBalance();
                return balance / 100 + DOT + (balance / 10 % 10) + balance % 10;
            }

        }, DELAY, period);
    }
}

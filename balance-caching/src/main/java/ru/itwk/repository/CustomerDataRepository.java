package ru.itwk.repository;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import ru.itwk.model.CustomerData;

import java.util.List;

public interface CustomerDataRepository extends CouchbaseRepository<CustomerData, String> {

    List<CustomerData> findCustomerDataByBalanceIsLessThan(long minimalBalance);
}

package ru.itwk.handler;

import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.itwk.model.CustomerData;
import ru.itwk.config.BeanName;

import java.util.LinkedList;
import java.util.List;

@Component(BeanName.CUSTOMER_DATA_HANDLER)
public class CustomerDataHandler extends DefaultHandler {

    private static final String TAG_STRONG = "strong";
    private static final String TAG_SPAN = "span";
    private static final String TAG_H7 = "h7";
    private static final String TAG_P = "p";
    private static final String TAG_HTML = "html";

    private List<CustomerData> tmpCustomerDataList = new LinkedList<>();
    private List<CustomerData> customerDataList;
    private String element;
    private CustomerData customerData;

    public List<CustomerData> getCustomerDataList() {
        return customerDataList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        element = qName;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        switch (element) {
            case TAG_H7:
                initCustomerData(new String(ch, start, length));
                break;
            case TAG_STRONG:
                customerData.setCustomerId(Long.parseLong(new String(ch, start, length)));
                break;
            case TAG_SPAN:
                customerData.setBalance(Long.parseLong(new String(ch, start, length)));
                break;
            default:
                break;
        }
    }

    private void initCustomerData(String email) {
        customerData = new CustomerData();
        customerData.setId(email);
        customerData.setEmail(email);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (TAG_P.equals(qName)) {
            tmpCustomerDataList.add(customerData);
        }
        if (TAG_HTML.equals(qName)) {
            customerDataList = tmpCustomerDataList;
            tmpCustomerDataList = new LinkedList<>();
        }
    }
}

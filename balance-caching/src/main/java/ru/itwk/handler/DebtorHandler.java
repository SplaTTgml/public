package ru.itwk.handler;


import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.itwk.config.BeanName;

@Component(BeanName.DEBTOR_HANDLER)
public class DebtorHandler extends DefaultHandler {

    private static final String TAG_EMAIL = "email";
    private static final String TAG_FIRST_NAME = "firstName";
    private static final String TAG_PATRONYMIC = "patronymic";
    private static final String DELIMITER = ";";
    private static final String SPACE = " ";

    private String firstName;
    private String patronymic;
    private String email;
    private String element;

    public String getAmqpMessage() {
        return email + DELIMITER + firstName + SPACE + patronymic;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        element = qName;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        switch (element) {
            case TAG_FIRST_NAME:
                firstName = new String(ch, start, length);
                break;
            case TAG_PATRONYMIC:
                patronymic = new String(ch, start, length);
                break;
            case TAG_EMAIL:
                email = new String(ch, start, length);
                break;
            default:
                break;
        }
    }
}

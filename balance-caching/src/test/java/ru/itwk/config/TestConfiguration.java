package ru.itwk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.itwk.handler.CustomerDataHandler;
import ru.itwk.handler.DebtorHandler;

@Configuration
public class TestConfiguration {

    @Bean
    public CustomerDataHandler customerDataMapHandler() {
        return new CustomerDataHandler();
    }

    @Bean
    public DebtorHandler customerNameHandler() {
        return new DebtorHandler();
    }
}

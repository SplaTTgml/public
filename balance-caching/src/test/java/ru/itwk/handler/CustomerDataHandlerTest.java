package ru.itwk.handler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;
import ru.itwk.config.TestConfiguration;
import ru.itwk.model.CustomerData;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class CustomerDataHandlerTest {

    public static final Long EXPECTED_CUSTOMER_ID = 1L;
    public static final String EXPECTED_EMAIL = "igor@gmail.com";
    public static final String TEST_STRING = "<html><body><p><h7>" + EXPECTED_EMAIL + "</h7><strong>" + EXPECTED_CUSTOMER_ID + "</strong><span>144</span></p></body></html>";

    @Autowired
    private CustomerDataHandler handler;
    private SAXParser saxParser;

    @Before
    public void setUp() throws Exception {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        saxParser = parserFactory.newSAXParser();
    }

    @Test
    public void getCustomerDataList() throws SAXException, IOException {
        saxParser.parse(new ByteArrayInputStream(TEST_STRING.getBytes()), handler);
        List<CustomerData> customerDataList = handler.getCustomerDataList();
        CustomerData actualCustomerData = customerDataList.get(0);
        assertEquals(EXPECTED_CUSTOMER_ID, actualCustomerData.getCustomerId());
        assertEquals(EXPECTED_EMAIL, actualCustomerData.getEmail());
    }
}
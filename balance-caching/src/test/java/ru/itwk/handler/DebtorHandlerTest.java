package ru.itwk.handler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;
import ru.itwk.config.TestConfiguration;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class DebtorHandlerTest {

    private static final String EXPECTED_FIRST_NAME = "Igor";
    private static final String EXPECTED_PATRONYMIC = "Mikhailovich";
    private static final String EXPECTED_EMAIL = "igor@gmail.com";
    private static final String TEST_STRING = "<customer><firstName>"
            + EXPECTED_FIRST_NAME +
            "</firstName><secondName>Yukhalko</secondName><patronymic>"
            + EXPECTED_PATRONYMIC +
            "</patronymic><email>" + EXPECTED_EMAIL + "</email></customer>";
    private static final String EXPECTED_AMQP_MESSAGE = EXPECTED_EMAIL + ";" + EXPECTED_FIRST_NAME + " " + EXPECTED_PATRONYMIC;

    @Autowired
    private DebtorHandler handler;
    private SAXParser saxParser;

    @Before
    public void setUp() throws Exception {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        saxParser = parserFactory.newSAXParser();
    }

    @Test
    public void getAmqpMessage() throws SAXException, IOException {
        saxParser.parse(new ByteArrayInputStream(TEST_STRING.getBytes()), handler);
        String amqpMessage = handler.getAmqpMessage();
        assertEquals(EXPECTED_AMQP_MESSAGE, amqpMessage);
    }
}